/// @func tw_sys_destroy();

var key = ds_map_find_first(global.tw_widgets);

repeat (ds_map_size(global.tw_widgets))
{
	ds_map_destroy(global.tw_widgets[? key]);
	
	key = ds_map_find_next(global.tw_widgets, key);
}

ds_map_destroy(global.tw_widgets);
