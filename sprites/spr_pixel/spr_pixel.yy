{
    "id": "b4cfb4e0-a7c5-43f6-b2eb-c67a0055dcd1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pixel",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "56411239-4d70-4159-bd51-59e3924fa81c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4cfb4e0-a7c5-43f6-b2eb-c67a0055dcd1",
            "compositeImage": {
                "id": "8049b330-c6ef-46eb-9ea4-b02dffe64833",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56411239-4d70-4159-bd51-59e3924fa81c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4eb7d0c0-3112-440b-82f0-75552e77a0d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56411239-4d70-4159-bd51-59e3924fa81c",
                    "LayerId": "4a3550ac-1713-4085-b7e6-58e1fe65f65d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "4a3550ac-1713-4085-b7e6-58e1fe65f65d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b4cfb4e0-a7c5-43f6-b2eb-c67a0055dcd1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}